#include <catch.hpp>

#include <ophidian/design/DesignFactory.h>
#include <ot/timer/timer.hpp>


TEST_CASE("Design factory: populate with simple lef, def, verilog.", "[design][Design][factory]")
{
    auto lef = ophidian::parser::Lef{"input_files/simple/simple.lef"};
    auto def = ophidian::parser::Def{"input_files/simple/simple.def"};
    auto verilog = ophidian::parser::Verilog{"input_files/simple/simple.v"};

    auto design = ophidian::design::Design{};

    ophidian::design::factory::make_design(design, def, lef, verilog);

    auto cell_u1 = design.netlist().find_cell_instance("u1");

    using dbu_t = ophidian::placement::Placement::unit_type;

    auto cell_u1_xpos = design.placement().location(cell_u1).x();
    auto cell_u1_ypos = design.placement().location(cell_u1).y();

    CHECK(cell_u1_xpos == dbu_t{3420});
    CHECK(cell_u1_ypos == dbu_t{6840});

    auto std_cell_u1 = design.netlist().std_cell(cell_u1);

    auto std_cell_u1_geometry = design.placement_library().geometry(std_cell_u1);

    auto first_box = std_cell_u1_geometry.front();

    CHECK(first_box.min_corner().x() == dbu_t{0});
    CHECK(first_box.min_corner().y() == dbu_t{0});

    using micron_t = ophidian::parser::Lef::micrometer_type;

    CHECK(first_box.max_corner().x() == dbu_t{ micron_t{1.140} * lef.micrometer_to_dbu_ratio()});
    CHECK(first_box.max_corner().y() == dbu_t{ micron_t{1.71} * lef.micrometer_to_dbu_ratio()});

    auto cell_u1_placed_geometry = design.placement().geometry(cell_u1);

    CHECK(cell_u1_placed_geometry.front().min_corner().x() == cell_u1_xpos);
    CHECK(cell_u1_placed_geometry.front().min_corner().y() == cell_u1_ypos);

    CHECK(cell_u1_placed_geometry.front().max_corner().x() == std_cell_u1_geometry.front().max_corner().x() + cell_u1_xpos);
    CHECK(cell_u1_placed_geometry.front().max_corner().y() == std_cell_u1_geometry.front().max_corner().y() + cell_u1_ypos);
}

TEST_CASE("Design factory: test design_factory_iccad17 with contest files", "[design][Design][factory]")
{
    auto lef = ophidian::parser::Lef{};

    lef.read_file("input_files/iccad17/pci_bridge32_a_md1/tech.lef");
    lef.read_file("input_files/iccad17/pci_bridge32_a_md1/cells_modified.lef");

    auto def = ophidian::parser::Def{};

    def.read_file("input_files/iccad17/pci_bridge32_a_md1/placed.def");

    auto design = ophidian::design::Design{};

    ophidian::design::factory::make_design_iccad2017(design, def, lef);

    CHECK(design.netlist().size_cell_instance() == 29521);
}

TEST_CASE("Design factory: test design_factory_tau2017 with contest files", "[design][Design][factory]")
{
    std::string circuitPath = "input_files/tau2015/simple/";
    auto earlyLiberty = ophidian::parser::Liberty{circuitPath+"simple_Early.lib"};
    auto lateLiberty = ophidian::parser::Liberty{circuitPath+"simple_Late.lib"};

    ot::Timer timer;
    timer.read_celllib(circuitPath+"simple_Early.lib", ot::Split::MIN);
    timer.read_celllib(circuitPath+"simple_Late.lib", ot::Split::MAX);
    timer.read_verilog(circuitPath+"simple.v");
    timer.read_spef(circuitPath+"simple.spef");
    timer.read_sdc(circuitPath+"simple.sdc");
    timer.update_timing();

    auto design = ophidian::design::Design{};
    ophidian::design::factory::make_design_tau2017(design, timer, earlyLiberty, lateLiberty);

    auto cell_u1 = design.netlist().find_cell_instance("u1");
    auto std_cell_u1 = design.netlist().std_cell(cell_u1);
    auto pin_u1_a = design.netlist().find_pin_instance("u1:a");
    auto std_cell_pin_a = design.netlist().std_cell_pin(pin_u1_a);
    auto net_inp1 = design.netlist().net(pin_u1_a);

    CHECK("u1" == design.netlist().name(cell_u1));
    CHECK("NAND2_X1" == design.standard_cells().name(std_cell_u1));
    CHECK("u1:a" == design.netlist().name(pin_u1_a));
    CHECK("NAND2_X1:a" == design.standard_cells().name(std_cell_pin_a));
    CHECK("inp1" == design.netlist().name(net_inp1));
}
