#include <catch.hpp>
#include <algorithm>
#include <ophidian/gate_sizing/Digraph.h>
#include <ophidian/design/DesignFactory.h>

using namespace ophidian;

class DigraphFixture{
public:
    design::Design m_design;
    ot::Timer m_openTimer;

    DigraphFixture(){
    std::string circuitPath = "input_files/tau2015/simple/";
    auto earlyLiberty = ophidian::parser::Liberty{circuitPath+"simple_Early.lib"};
    auto lateLiberty = ophidian::parser::Liberty{circuitPath+"simple_Late.lib"};

    m_openTimer.read_celllib(circuitPath+"simple_Early.lib", ot::Split::MIN);
    m_openTimer.read_celllib(circuitPath+"simple_Late.lib", ot::Split::MAX);
    m_openTimer.read_verilog(circuitPath+"simple.v");
    m_openTimer.read_spef(circuitPath+"simple.spef");
    m_openTimer.read_sdc(circuitPath+"simple.sdc");
    m_openTimer.update_timing();

    auto design = ophidian::design::Design{};
    ophidian::design::factory::make_design_tau2017(m_design, m_openTimer, earlyLiberty, lateLiberty);
    }
};

TEST_CASE_METHOD(DigraphFixture, "GateSizing: test for digraph primary inputs and outputs.", "[digraph][gate_sizing]"){
    auto digraph = gate_sizing::Digraph(m_design.netlist(), m_design.standard_cells(), m_design.early_timingLibrary(), m_design.late_timingLibrary());

    auto primaryInputNames = digraph.primaryInputNames();
    std::vector<std::string> expectedInputs = {"inp1", "inp2", "tau2015_clk"};
    REQUIRE(std::is_permutation(primaryInputNames.begin(), primaryInputNames.end(), expectedInputs.begin()));

    auto primaryOutputNames = digraph.primaryOutputNames();
    std::vector<std::string> expectedOutputs = {"out", "u4"};
    REQUIRE(std::is_permutation(primaryOutputNames.begin(), primaryOutputNames.end(), expectedOutputs.begin()));
}

TEST_CASE_METHOD(DigraphFixture, "GateSizing: test for digraph fanin and fanout.", "[digraph][gate_sizing]"){
    auto digraph = gate_sizing::Digraph(m_design.netlist(), m_design.standard_cells(), m_design.early_timingLibrary(), m_design.late_timingLibrary());

    auto f1Fanouts = digraph.fanout("f1");
    std::vector<std::string> expectedFanouts = {"u2", "u4"};
    REQUIRE(std::is_permutation(f1Fanouts.begin(),f1Fanouts.end(), expectedFanouts.begin()));

    auto u1Fanins = digraph.fanin("u1");
    std::vector<std::string> expectedFanins = {"inp1", "inp2"};
    REQUIRE(std::is_permutation(u1Fanins.begin(), u1Fanins.end(), expectedFanins.begin()));
}

TEST_CASE_METHOD(DigraphFixture, "Digraph: Test for digraph initial kkt multipliers.", "[digraph][gate_sizing]"){
    auto digraph = gate_sizing::Digraph(m_design.netlist(), m_design.standard_cells(), m_design.early_timingLibrary(), m_design.late_timingLibrary());
    digraph.initializeMultipliers();
    REQUIRE(digraph.earlyLambdaMultiplier("inp1").aggregate() == 0.125);
    REQUIRE(digraph.lateLambdaMultiplier("u3:a").aggregate() == 0.5);
    REQUIRE(digraph.earlyLambdaMultiplier("u4:a").aggregate() == 0.25);
    REQUIRE(digraph.lateLambdaMultiplier("u4:b").aggregate() == 0.25);
    REQUIRE(digraph.lateLambdaMultiplier("SinkNode").aggregate() == 1);
    REQUIRE(digraph.earlyLambdaMultiplier("SourceNode").aggregate() == 1);
    REQUIRE(digraph.earlyLambdaMultiplier("f1:ck").aggregate() == 0.75);
    REQUIRE(digraph.lateLambdaMultiplier("f1:ck").aggregate() == 0.75);
    REQUIRE(digraph.earlyBetaMultiplier("f1") == 1.0);
    REQUIRE(digraph.lateBetaMultiplier("f1") == 1.0);
}

