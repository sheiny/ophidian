#include <catch.hpp>
#include <ophidian/gate_sizing/GateSizing.h>
#include <ophidian/design/DesignFactory.h>
#include <ophidian/timing/Timer.h>

using namespace ophidian;

class GateSizingFixture{
public:
    design::Design m_design;
    ot::Timer m_openTimer;

    GateSizingFixture(){
    std::string circuitPath = "input_files/tau2015/simple/";
    auto earlyLiberty = ophidian::parser::Liberty{circuitPath+"simple_Early.lib"};
    auto lateLiberty = ophidian::parser::Liberty{circuitPath+"simple_Late.lib"};

    m_openTimer.read_celllib(circuitPath+"simple_Early.lib", ot::Split::MIN);
    m_openTimer.read_celllib(circuitPath+"simple_Late.lib", ot::Split::MAX);
    m_openTimer.read_verilog(circuitPath+"simple.v");
    m_openTimer.read_spef(circuitPath+"simple.spef");
    m_openTimer.read_sdc(circuitPath+"simple.sdc");

    /*std::string circuitPath = "input_files/testcases_v2.0/leon3mp_iccad.tau/design/";
    auto earlyLiberty = ophidian::parser::Liberty{"input_files/testcases_v2.0/lib/NangateOpenCellLibrary_fast.lib"};
    auto lateLiberty = ophidian::parser::Liberty{"input_files/testcases_v2.0/lib/NangateOpenCellLibrary_slow.lib"};

    m_openTimer.read_celllib("input_files/testcases_v2.0/lib/NangateOpenCellLibrary_fast.lib", ot::Split::MIN);
    m_openTimer.read_celllib("input_files/testcases_v2.0/lib/NangateOpenCellLibrary_slow.lib", ot::Split::MAX);
    m_openTimer.read_verilog(circuitPath+"/leon3mp_iccad.v");
    m_openTimer.read_spef(circuitPath+"/leon3mp_iccad.spef");
    m_openTimer.read_sdc(circuitPath+"/leon3mp_iccad.sdc");*/

    /*std::string circuitPath = "input_files/testcases_v2.0/s1196.tau/design/";
    auto earlyLiberty = ophidian::parser::Liberty{"input_files/testcases_v2.0/lib/NangateOpenCellLibrary_fast.lib"};
    auto lateLiberty = ophidian::parser::Liberty{"input_files/testcases_v2.0/lib/NangateOpenCellLibrary_slow.lib"};

    m_openTimer.read_celllib("input_files/testcases_v2.0/lib/NangateOpenCellLibrary_fast.lib", ot::Split::MIN);
    m_openTimer.read_celllib("input_files/testcases_v2.0/lib/NangateOpenCellLibrary_slow.lib", ot::Split::MAX);
    m_openTimer.read_verilog(circuitPath+"/s1196.v");
    m_openTimer.read_spef(circuitPath+"/s1196.spef");
    m_openTimer.read_sdc(circuitPath+"/s1196.sdc");*/

    m_openTimer.update_timing();

    auto design = ophidian::design::Design{};
    ophidian::design::factory::make_design_tau2017(m_design, m_openTimer, earlyLiberty, lateLiberty);
    }
};

TEST_CASE_METHOD(GateSizingFixture, "GateSizing: test for greedy leakage minimization.", "[gate_sizing]"){
    auto timer = timing::OpenTimer(m_design, m_openTimer);
    auto before_tns = timer.tns();
    auto sizer = gate_sizing::GateSizing(m_design, timer);
    auto after_tns = timer.tns();
    REQUIRE(after_tns <= before_tns);
}

/*TEST_CASE_METHOD(GateSizingFixture, "GateSizing: test for LRS solve.", "[gate_sizing][lrs]"){
    auto timer = timing::OpenTimer(m_design, m_openTimer);
    auto sizer = gate_sizing::GateSizing(m_design, timer);
    auto greedy_tns = timer.tns();
    sizer.discrete_sizing();
    auto lrs_tns = timer.tns();
    REQUIRE(greedy_tns != lrs_tns);
}*/
