/*
 * Copyright 2017 Ophidian
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless CHECKd by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

#include <catch.hpp>
#include <ophidian/timing/Timer.h>
#include <ot/timer/timer.hpp>
#include <ophidian/design/DesignFactory.h>

using namespace ophidian;

TEST_CASE("Timer: test of integration with OpenTimer.", "[timing][opentimer]"){
    std::string circuitPath = "input_files/tau2015/simple/";
    auto openTimer = ot::Timer{};
    openTimer.read_celllib(circuitPath+"simple_Early.lib", ot::Split::MIN);
    openTimer.read_celllib(circuitPath+"simple_Late.lib", ot::Split::MAX);
    openTimer.read_verilog(circuitPath+"simple.v");
    openTimer.read_spef(circuitPath+"simple.spef");
    openTimer.read_sdc(circuitPath+"simple.sdc");
    openTimer.update_timing();

    auto earlyLiberty = ophidian::parser::Liberty{circuitPath+"simple_Early.lib"};
    auto lateLiberty = ophidian::parser::Liberty{circuitPath+"simple_Late.lib"};

    auto design = design::Design{};
    design::factory::make_design_tau2017(design, openTimer, earlyLiberty, lateLiberty);
    auto ophidianTimer = timing::OpenTimer(design, openTimer);
    CHECK(ophidianTimer.wns() == openTimer.report_slack(std::string("u1:o"), timing::OpenTimer::split_type::MAX, timing::OpenTimer::transition_type::FALL));
    CHECK(ophidianTimer.tns() == Approx(-590.861));
}

