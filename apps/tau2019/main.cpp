#include <iostream>
#include <fstream>
#include <ophidian/gate_sizing/GateSizing.h>
#include <ophidian/design/DesignFactory.h>
#include <ophidian/timing/Timer.h>

using namespace ophidian;

struct RunTau{
    std::string verilog, spef, early, late;
    void read_tau(std::string file_path){
        std::ifstream file;
        file.open (file_path);
        std::string word;
        file_path = file_path.substr(0, file_path.find_last_of("/")+1);
        std::cout<<file_path<<std::endl;
        while(file >> word){
            auto file_t = word.substr(word.find("."), word.size());
            if(file_t == ".v")
                verilog = file_path+word;
            else if(file_t == ".spef")
                spef = file_path+word;
            else if(file_t == ".lib"){
                if(word.find("Early") != std::string::npos || word.find("fast") != std::string::npos || word.find("low_temp") != std::string::npos)
                    early = file_path+word;
                else
                    late = file_path+word;
            }
        }
        file.close();
    }
};

int main(int argc, char *argv[])
{
// ./ophidian_tau ../../test/input_files/tau2015/simple/simple.tau2015 ../../test/input_files/tau2015/simple/simple.sdc out.v out.spef out.out
// ./ophidian_tau ../../test/input_files/testcases_v2.0/s1196.tau/s1196.tau2019 ../../test/input_files/testcases_v2.0/s1196.tau/design/s1196.sdc out.v out.spef out.out
    if(argc == 6){
        RunTau runner;
        auto design = ophidian::design::Design{};
        ot::Timer openTimer;

        runner.read_tau(argv[1]);
        auto earlyLiberty = ophidian::parser::Liberty{runner.early};
        auto lateLiberty = ophidian::parser::Liberty{runner.late};

        openTimer.read_celllib(runner.early, ot::Split::MIN);
        openTimer.read_celllib(runner.late, ot::Split::MAX);
        openTimer.read_verilog(runner.verilog);
        openTimer.read_spef(runner.spef);
        openTimer.read_sdc(argv[2]);

        openTimer.update_timing();
        ophidian::design::factory::make_design_tau2017(design, openTimer, earlyLiberty, lateLiberty);

        auto timer = timing::OpenTimer(design, openTimer);
        auto sizer = gate_sizing::GateSizing(design, timer);

        sizer.discrete_sizing(argv[3], argv[4], argv[5], runner.late);
    }else{
        std::cout<<"Error, usage: ./bin <.tau2019> <.sdc> <output.v> <output.spef> <output file>" << std::endl;
    }
}
