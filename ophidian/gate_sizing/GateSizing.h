/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#ifndef OPHIDIAN_GATE_SIZING_H
#define OPHIDIAN_GATE_SIZING_H

#include <ophidian/gate_sizing/Digraph.h>
#include <ophidian/design/Design.h>
#include <ophidian/timing/Timer.h>
#include <ophidian/parser/Liberty.h>
#include <unordered_map>
#include <fstream>

namespace ophidian::gate_sizing{
class GateSizing{
    public:
        using capacitance_unit_type  = ophidian::timing::Library::capacitance_unit_type;
        using transition_type = ophidian::timing::OpenTimer::transition_type;
        using split_type = ophidian::timing::OpenTimer::split_type;
        using timing_type = ophidian::timing::OpenTimer::transition_type;
        using cell_instance_type = ophidian::circuit::Netlist::cell_instance_type;
        using pin_instance_name_type = std::string;
        using net_type = ophidian::circuit::Netlist::net_type;
        using pin_instance_type = ophidian::circuit::Netlist::pin_instance_type;
        using unateness_type = ophidian::parser::Liberty::Timing::unateness;
        using split_t = ophidian::timing::OpenTimer::split_type;
        using transition_t = ophidian::timing::OpenTimer::transition_type;
        using transitions_type = ophidian::util::Transitions<double>;

        GateSizing() = delete;

        GateSizing(const GateSizing&) = default;
        GateSizing& operator=(const GateSizing&) = default;

        GateSizing(GateSizing&&) = default;
        GateSizing& operator=(GateSizing&&) = default;

        GateSizing(design::Design& design, timing::OpenTimer &timer);

        void discrete_sizing(std::string out_verilog, std::string out_spef, std::string out_file, std::string late_lib);
    private:
        inline bool is_connected(pin_instance_type pin) const;
        double net_timing_cost(pin_instance_name_type input_name, pin_instance_name_type output_name);
        double cell_timing_cost(std::string cell_instance_name);
        void minimize_leakage();
        void map_all_std_cell_options();
        void update_power_importance();
        void solve_lrs();
        double timing_cost(std::string cell_instance_name);
        double capacitance_cost(std::string instance_name);
        void fix_timing_violations();
        void solve_ldp();
        void update_lambdas_pos();
        void update_lambdas_cells();
        pin_instance_type worst_arrival_cell_out_pin(cell_instance_type cell);
        void update_lambda_timing_criticality(std::vector<pin_instance_name_type> inputs, pin_instance_type output_pin);
        void distribute_lambdas();
        void print_graph();
        void write_graph(std::string file_name);
        void print_cost();
        double power_leakage_cost();

        timing::OpenTimer &m_OpenTimer;
        gate_sizing::Digraph m_digraph;
        design::Design & m_design;
        timing::TimingArcs &m_early_timing_arcs, &m_late_timing_arcs;
        double m_power_importance = 1.0;
        std::unordered_map<std::string, std::vector<std::string>> m_cell_name2options;
};
}
#endif // OPHIDIAN_GATE_SIZING_H
