/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#include "Digraph.h"

namespace ophidian::gate_sizing{

inline bool Digraph::is_output_cell_pin(const ophidian::circuit::PinInstance &pin_instance) const{
    return m_std_cells.direction(m_netlist.std_cell_pin(pin_instance)) == ophidian::circuit::PinDirection::OUTPUT;
}

inline bool Digraph::is_cell_input_pin(const ophidian::circuit::PinInstance &pin_instance) const{
    return m_std_cells.direction(m_netlist.std_cell_pin(pin_instance)) == ophidian::circuit::PinDirection::INPUT;
}

Digraph::pin_instance_type Digraph::net_source_pin(Digraph::net_type net){
    auto pin_instance = ophidian::circuit::PinInstance();
    for(auto pin : m_netlist.pins(net)){
        if(is_output_pad(pin))
            continue;
        else if(is_input_pad(pin)){
            pin_instance = pin;
            break;
        }else if(is_output_cell_pin(pin)){
            pin_instance = pin;
            break;
       }
    }
    return pin_instance;
}

std::vector<Digraph::pin_instance_type> Digraph::net_targets(Digraph::net_type net){
    std::vector<pin_instance_type> targets;
    for(auto pin : m_netlist.pins(net)){
        if(is_output_pad(pin))
            targets.push_back(pin);
        else if(is_input_pad(pin)){
            continue;
        }else if(is_cell_input_pin(pin)){
            targets.push_back(pin);
       }
    }
    return targets;
}

bool Digraph::is_po(Digraph::node_name_type nodeName){
    auto fanout_nodes = fanout(nodeName);
    for(auto nodeName : fanout_nodes)
        if(nodeName == "SinkNode")
            return true;
    return false;
}

std::vector<Digraph::pin_name_type> Digraph::cell_input_pins(const ophidian::circuit::CellInstance cellInstance) const{
    std::vector<pin_name_type> pin_names;
    auto std_cell = m_netlist.std_cell(cellInstance);
    for(auto pin_instance : m_netlist.pins(cellInstance)){
        if(is_sequential_input_pin(pin_instance))
            continue;
        else if(m_std_cells.direction(m_netlist.std_cell_pin(pin_instance)) == ophidian::circuit::PinDirection::INPUT || m_earlyLibrary.pin_clock(m_netlist.std_cell_pin(pin_instance)))
            pin_names.push_back(m_netlist.name(pin_instance));
    }
    return pin_names;
}

std::vector<Digraph::pin_instance_type> Digraph::cell_output_pins(const ophidian::circuit::CellInstance cellInstance){
    std::vector<Digraph::pin_instance_type> output_pins;
    for(auto pin : m_netlist.pins(cellInstance))
        if(m_std_cells.direction(m_netlist.std_cell_pin(pin)) == ophidian::circuit::PinDirection::OUTPUT)
            output_pins.push_back(pin);
    return output_pins;
}

Digraph::Digraph(const ophidian::circuit::Netlist &netlist, const ophidian::circuit::StandardCells &stdCells, const ophidian::timing::Library &earlyLibrary, const ophidian::timing::Library &lateLibrary):m_netlist(netlist), m_digraph(), m_node2name(m_digraph), m_earlyLibrary(earlyLibrary), m_lateLibrary(lateLibrary), m_std_cells(stdCells){
std::vector<node_name_type> primaryInputNames, primaryOutputNames;

m_sourceNode = m_digraph.addNode();
m_node2name[m_sourceNode] = "SourceNode";
m_name2node["SourceNode"] = m_sourceNode;

m_sinkNode = m_digraph.addNode();
m_node2name[m_sinkNode] = "SinkNode";
m_name2node["SinkNode"] = m_sinkNode;

for(auto net_it = m_netlist.begin_net(); net_it != m_netlist.end_net(); net_it++){
    node_name_type fromName;
    pin_name_type fromPinName;
    for(auto pin_instance : m_netlist.pins(*net_it)){//get net source pin
        fromName = m_netlist.name(pin_instance);
        fromPinName = fromName;
        if(is_output_pad(pin_instance))
            continue;
        else if(is_input_pad(pin_instance)){
	        primaryInputNames.push_back(fromName);
            break;
        }else if(is_output_cell_pin(pin_instance)){
            fromName = m_netlist.name(m_netlist.cell(pin_instance));
            break;
       }
    }
    auto fromNode = (m_name2node.find(fromName) == m_name2node.end()) ? m_digraph.addNode() : m_name2node[fromName];
    m_node2name[fromNode] = fromName;
    m_name2node[fromName] = fromNode;

    for(auto pin_instance : m_netlist.pins(*net_it)){
        node_name_type targetName = m_netlist.name(pin_instance);
        node_name_type targetPinName = targetName;
        if(is_input_pad(pin_instance))
            continue;
        else if(is_output_pad(pin_instance))
            primaryOutputNames.push_back(targetName);
        else if(is_output_cell_pin(pin_instance))
            continue;
        else if(is_sequential_input_pin(pin_instance)){
            primaryOutputNames.push_back(fromName);
            continue;
        }else
            targetName = netlist.name(m_netlist.cell(pin_instance));//is input cell pin
        auto targetNode = (m_name2node.find(targetName) == m_name2node.end()) ? m_digraph.addNode() : m_name2node[targetName];
        m_node2name[targetNode] = targetName;
        m_name2node[targetName] = targetNode;
        m_digraph.addArc(fromNode, targetNode);
    }
}

for(auto primaryInputName : primaryInputNames)
    m_digraph.addArc(m_sourceNode, m_name2node[primaryInputName]);

for(auto primaryOutputName : primaryOutputNames)
    m_digraph.addArc(m_name2node[primaryOutputName], m_sinkNode);

/*
std::cout<<"Printing graph"<<std::endl;
for(lemon::ListDigraph::NodeIt nodeIt(m_digraph); nodeIt != lemon::INVALID; ++nodeIt)
    for(lemon::ListDigraph::OutArcIt arcIt(m_digraph, nodeIt); arcIt != lemon::INVALID; ++arcIt)
        std::cout<<m_node2name[nodeIt]<<" -> "<<m_node2name[m_digraph.oppositeNode(nodeIt, arcIt)]<<std::endl;
*/
m_topology = topologicalSort();
}

const std::vector<Digraph::node_name_type> Digraph::topologicalSort(){
    m_topology.resize(m_digraph.maxNodeId()+1);
    topological_map_type topological_sort(m_digraph);
    bool is_dag = lemon::checkedTopologicalSort(m_digraph, topological_sort);
    assert(is_dag);
    for(lemon::ListDigraph::NodeIt nodeIt(m_digraph); nodeIt != lemon::INVALID; ++nodeIt)
        m_topology.at(topological_sort[nodeIt]) = m_node2name[nodeIt];
    return m_topology;
}

std::vector<Digraph::node_name_type> Digraph::primaryInputNames() const {
    return fanout(m_node2name[m_sourceNode]);
}

std::vector<Digraph::node_name_type> Digraph::primaryOutputNames() const {
    return fanin(m_node2name[m_sinkNode]);
}

std::vector<Digraph::node_name_type> Digraph::fanin(const node_name_type &nodeName) const {
    std::vector<node_name_type> fanins;
    auto currentNode = m_name2node.at(nodeName);
    for(lemon::ListDigraph::InArcIt arcIt(m_digraph, currentNode); arcIt != lemon::INVALID; ++arcIt)
        fanins.push_back(m_node2name[m_digraph.oppositeNode(currentNode, arcIt)]);
    std::sort(fanins.begin(), fanins.end());
    fanins.erase(std::unique(fanins.begin(), fanins.end()), fanins.end());
    return fanins;
}

std::vector<Digraph::node_name_type> Digraph::fanout(const node_name_type &nodeName) const {
    std::vector<node_name_type> fanouts;
    auto currentNode = m_name2node.at(nodeName);
    for(lemon::ListDigraph::OutArcIt arcIt(m_digraph, currentNode); arcIt != lemon::INVALID; ++arcIt)
        fanouts.push_back(m_node2name[m_digraph.oppositeNode(currentNode, arcIt)]);
    std::sort(fanouts.begin(), fanouts.end());
    fanouts.erase(std::unique(fanouts.begin(), fanouts.end()), fanouts.end());
    return fanouts;
}

void Digraph::initializeMultipliers(){
    std::unordered_map<node_name_type, double> node2input_lambda;
    for(auto mapIt : m_name2node){
        node2input_lambda[mapIt.first] = 0;
        m_name2beta_early[mapIt.first] = 1.0;
        m_name2beta_late[mapIt.first] = 1.0;
    }
    node2input_lambda[m_node2name[m_sinkNode]] = 1;

    for(auto node_name_it = m_topology.rbegin(); node_name_it != m_topology.rend(); ++node_name_it){
        std::vector<node_name_type> lambda_points = (is_cell_instance(*node_name_it)) ? cell_input_pins(m_netlist.find_cell_instance(*node_name_it)) : std::vector<node_name_type>{*node_name_it};
        auto input_lambda = node2input_lambda[*node_name_it]/lambda_points.size();

        for(auto lambda_point : lambda_points){
            m_name2lambda_late[lambda_point] = transitions_type(input_lambda/2, input_lambda/2);
            m_name2lambda_early[lambda_point] = transitions_type(input_lambda/2, input_lambda/2);
        }
        auto node_fanin = fanin(*node_name_it);
        input_lambda = node2input_lambda[*node_name_it]/node_fanin.size();
        for(auto fanin_name : node_fanin)
            node2input_lambda[fanin_name] += input_lambda;
    }
}

//TODO add test for invalid entries and how maps should handle then.
Digraph::transitions_type Digraph::earlyLambdaMultiplier(pin_name_type pinName){
    return m_name2lambda_early[pinName];
}

Digraph::transitions_type Digraph::lateLambdaMultiplier(pin_name_type pinName){
    return m_name2lambda_late[pinName];
}

double Digraph::earlyBetaMultiplier(node_name_type nodeName){
    return m_name2beta_early[nodeName];
}

double Digraph::lateBetaMultiplier(node_name_type nodeName){
    return m_name2beta_late[nodeName];
}


void Digraph::setLateBetaMultiplier(Digraph::node_name_type nodeName, double betaMultiplier){
    m_name2beta_late[nodeName] = betaMultiplier;
}

std::vector<Digraph::node_name_type> Digraph::topology() const{
    return m_topology;
}

void Digraph::setLateLambdaMultiplier(Digraph::pin_name_type pinName, Digraph::transitions_type t){
    m_name2lambda_late[pinName] = t;
}

}
