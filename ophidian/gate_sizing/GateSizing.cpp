/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#include "GateSizing.h"

namespace ophidian::gate_sizing{
inline bool GateSizing::is_connected(pin_instance_type pin) const{
    auto net = m_design.netlist().net(pin);
    return net != ophidian::circuit::Net();
}

GateSizing::GateSizing(design::Design& design, timing::OpenTimer &timer):m_design(design), m_digraph(design.netlist(), design.standard_cells(), design.early_timingLibrary(), design.late_timingLibrary()), m_early_timing_arcs(design.early_timingLibrary().m_timing_arcs), m_late_timing_arcs(design.late_timingLibrary().m_timing_arcs), m_OpenTimer(timer){
    m_digraph.initializeMultipliers();
    map_all_std_cell_options();
    minimize_leakage();
}

void GateSizing::discrete_sizing(std::string out_verilog, std::string out_spef, std::string out_file, std::string late_lib){
    double episolon = 0.5, previous_power_cost;
    for(unsigned int i = 0; i < 20; i++){
        m_OpenTimer.update_timing();
        update_power_importance();
        solve_lrs();
        fix_timing_violations();
        solve_ldp();
        distribute_lambdas();
        auto power_cost = power_leakage_cost();
        //print_cost();
        if(i != 0)
            if(std::abs(previous_power_cost - power_cost) < episolon)
                break;
            else
                previous_power_cost = power_cost;
        m_OpenTimer.dump_output(out_file, late_lib);
        m_OpenTimer.dump_verilog(out_verilog);
        m_OpenTimer.dump_spef(out_spef);
    }
}

void GateSizing::minimize_leakage(){
    for(auto cell_instance_it = m_design.netlist().begin_cell_instance(); cell_instance_it != m_design.netlist().end_cell_instance(); ++cell_instance_it){
        auto instance_name = m_design.netlist().name(*cell_instance_it);
        auto std_cell = m_design.netlist().std_cell(*cell_instance_it);
        auto std_cell_name = m_design.standard_cells().name(std_cell);
        auto availableModels = m_cell_name2options[std_cell_name.substr(0, std_cell_name.find("_"))];
        auto min_capacitance = std::numeric_limits<capacitance_unit_type>::max();

        if(m_design.late_timingLibrary().cell_sequential(std_cell) || std_cell_name.find("CLKBUF") != std::string::npos)
            continue;

        std::string best_candidate;
        for(auto model_name : availableModels){
            auto candidate_capacitance = m_design.late_timingLibrary().max_capacitance(m_design.standard_cells().find_cell(model_name));
            if(candidate_capacitance <= min_capacitance){
                min_capacitance = candidate_capacitance;
                best_candidate = model_name;
            }
        }
        m_OpenTimer.repower_cell_instance(instance_name, best_candidate);
    }
}

void GateSizing::map_all_std_cell_options(){
    for(auto std_cell : m_design.standard_cells().range_cell()){
        auto std_cell_name = m_design.standard_cells().name(std_cell);
        auto std_cell_model_name = std_cell_name.substr(0, std_cell_name.find("_"));
        std::vector<std::string> availableModels;
        if(m_cell_name2options.find(std_cell_model_name)!= m_cell_name2options.end())
            availableModels = m_cell_name2options[std_cell_model_name];
        availableModels.push_back(std_cell_name);
	    m_cell_name2options[std_cell_model_name] = availableModels;
    }
}

void GateSizing::update_power_importance(){
    float worst_arrival = std::numeric_limits<float>::min();
    auto clock_period = m_OpenTimer.period();
    for(auto output_name : m_digraph.primaryOutputNames()){
        worst_arrival = std::max(worst_arrival, m_OpenTimer.at(output_name, split_type::MAX, transition_type::RISE));
        worst_arrival = std::max(worst_arrival, m_OpenTimer.at(output_name, split_type::MAX, transition_type::FALL));
        /*worst_arrival = std::max(worst_arrival, m_OpenTimer.at(output_name, split_type::MIN, transition_type::RISE));
        worst_arrival = std::max(worst_arrival, m_OpenTimer.at(output_name, split_type::MIN, transition_type::FALL));*/
    }
    m_power_importance *= clock_period/worst_arrival;
}

void GateSizing::solve_lrs(){
    for(auto instanceName : m_digraph.topology()){
        if(!m_digraph.is_cell_instance(instanceName))
            continue;

        auto cell_instance = m_design.netlist().find_cell_instance(instanceName);
        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto std_cell_name = m_design.standard_cells().name(std_cell);
        if(m_design.late_timingLibrary().cell_sequential(std_cell) || std_cell_name.find("CLKBUF") != std::string::npos)
            continue;
        auto std_cell_model = std_cell_name.substr(0, std_cell_name.find("_"));
        double best_cost = std::numeric_limits<double>::max();

        for(auto option : m_cell_name2options[std_cell_model]){
            double actual_cost = 0.0;
            auto previous_option = m_design.standard_cells().name(m_design.netlist().std_cell(cell_instance));

            m_OpenTimer.repower_cell_instance(instanceName, option);

            for(auto fanin_name : m_digraph.fanin(instanceName)){
                actual_cost += capacitance_cost(fanin_name);
                if(!m_digraph.is_cell_instance(fanin_name))
                    continue;
                auto fanin_instance = m_design.netlist().find_cell_instance(fanin_name);
                actual_cost += timing_cost(fanin_name);
            }

            actual_cost += timing_cost(instanceName);
            actual_cost += m_power_importance * m_design.late_timingLibrary().cell_leakage_power(m_design.standard_cells().find_cell(option));
            actual_cost += capacitance_cost(instanceName);


            /*for(auto fanout_name : m_digraph.fanout(instanceName)){
                if(!m_digraph.is_cell_instance(fanout_name))
                    continue;
                auto fanout_instance = m_design.netlist().find_cell_instance(fanout_name);
                //actual_cost += slew_degradation_on_timing_arc_cost(fanout_name);TODO
            }*/

            if(actual_cost > best_cost)
                m_OpenTimer.repower_cell_instance(instanceName, previous_option);
            else
                best_cost = actual_cost;
        }
    }
}

double GateSizing::timing_cost(std::string cell_instance_name){
    auto cell_instance = m_design.netlist().find_cell_instance(cell_instance_name);
    auto cell_input_pins = m_digraph.cell_input_pins(cell_instance);

    double timing_cost = 0.0;

    for(auto pin_name : cell_input_pins){
        auto pin_instance = m_design.netlist().find_pin_instance(pin_name);
        if(!is_connected(pin_instance))
            continue;
        auto net = m_design.netlist().net(pin_instance);
        auto source_pin = m_digraph.net_source_pin(net);
        auto source_pin_name = m_design.netlist().name(source_pin);
        timing_cost += net_timing_cost(source_pin_name, pin_name);
    }
    timing_cost += cell_timing_cost(cell_instance_name);
    return timing_cost;
}

double GateSizing::cell_timing_cost(std::string cell_instance_name){
    auto cell_instance = m_design.netlist().find_cell_instance(cell_instance_name);
    auto cell_input_pins = m_digraph.cell_input_pins(cell_instance);
    auto cell_output_pins = m_digraph.cell_output_pins(cell_instance);

    double timing_cost = 0.0;

    for(auto out_pin : cell_output_pins){
        if(!is_connected(out_pin))
            continue;
        auto out_pin_name = m_design.netlist().name(out_pin);
        for(auto inp_name : cell_input_pins){
            auto t_arcs = m_OpenTimer.timing_arcs(inp_name, out_pin_name, split_t::MAX);
            for(auto transition : {transition_t::RISE, transition_t::FALL}){
                auto out_net = m_design.netlist().net(out_pin);
                auto load = m_OpenTimer.load(m_design.netlist().name(out_net), split_t::MAX, transition);
                auto inp_slew = m_OpenTimer.slew(inp_name, split_t::MAX, transition);
                auto delay = m_OpenTimer.worst_delay(t_arcs, transition, inp_slew, load);
                auto lambda = (transition == transition_t::RISE) ? m_digraph.lateLambdaMultiplier(inp_name).rise() : m_digraph.lateLambdaMultiplier(inp_name).fall();
                timing_cost += delay * lambda;
            }
        }
    }
    return timing_cost;
}

double GateSizing::net_timing_cost(pin_instance_name_type input_name, pin_instance_name_type output_name){
    double rise_delay, fall_delay, net_timing_cost = 0.0;
    for(auto split : {/*split::MIN,*/ split_type::MAX}){
        rise_delay = m_OpenTimer.at(output_name, split, transition_t::RISE) - m_OpenTimer.at(input_name, split, transition_t::RISE);
        fall_delay = m_OpenTimer.at(output_name, split, transition_t::FALL) - m_OpenTimer.at(input_name, split, transition_t::FALL);
        if(split == split_t::MIN){
            net_timing_cost += m_digraph.earlyLambdaMultiplier(output_name).rise() * rise_delay;
            net_timing_cost += m_digraph.earlyLambdaMultiplier(output_name).fall() * fall_delay;
        }else{
            net_timing_cost += m_digraph.lateLambdaMultiplier(output_name).rise() * rise_delay;
            net_timing_cost += m_digraph.lateLambdaMultiplier(output_name).fall() * fall_delay;
        }
    }
    return net_timing_cost;
}

double GateSizing::capacitance_cost(std::string instance_name){
    double cost = 0;
    if(m_digraph.is_cell_instance(instance_name)){
        auto beta_multiplier = m_digraph.lateBetaMultiplier(instance_name);
        auto cell_instance = m_design.netlist().find_cell_instance(instance_name);
        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto max_capacitance = m_design.late_timingLibrary().max_capacitance(std_cell);
        for(auto out_pin : m_digraph.cell_output_pins(m_design.netlist().find_cell_instance(instance_name))){
            if(!is_connected(out_pin))
                continue;
            auto net_name = m_design.netlist().name(m_design.netlist().net(out_pin));
            auto out_capacitance_r = m_OpenTimer.load(net_name, split_type::MAX, transition_t::RISE);
            auto out_capacitance_f = m_OpenTimer.load(net_name, split_type::MAX, transition_t::FALL);
            auto out_capacitance = util::femtofarad_t(std::max(out_capacitance_r, out_capacitance_f));
            cost += (beta_multiplier * units::unit_cast<double>(out_capacitance - max_capacitance));
        }
    }else{
        double max_capacitance = 60;
        auto pin_instance = m_design.netlist().find_pin_instance(instance_name);
        auto net_name = m_design.netlist().name(m_design.netlist().net(pin_instance));
        auto out_capacitance_r = m_OpenTimer.load(net_name, split_type::MAX, transition_t::RISE);
        auto out_capacitance_f = m_OpenTimer.load(net_name, split_type::MAX, transition_t::FALL);
        auto out_capacitance = std::max(out_capacitance_r, out_capacitance_f);
        auto beta_multiplier = m_digraph.lateBetaMultiplier(instance_name);
        cost += (beta_multiplier * (out_capacitance - max_capacitance));
    }
    return cost;
}

void GateSizing::fix_timing_violations(){
    auto topology = m_digraph.topology();
    double best_cost = std::numeric_limits<double>::max();
    std::string best_option;
    for(auto instance_name_it = topology.rbegin(); instance_name_it != topology.rend(); instance_name_it++){
        if(!m_digraph.is_cell_instance(*instance_name_it))
            continue;
        auto cell_instance = m_design.netlist().find_cell_instance(*instance_name_it);
        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto std_cell_name = m_design.standard_cells().name(std_cell);
        if(m_design.late_timingLibrary().cell_sequential(std_cell) || std_cell_name.find("CLKBUF") != std::string::npos)
            continue;
        util::femtofarad_t max_out_capacitance;
        for(auto out_pin : m_digraph.cell_output_pins(m_design.netlist().find_cell_instance(*instance_name_it))){
            if(!is_connected(out_pin))
                continue;
            auto net_name = m_design.netlist().name(m_design.netlist().net(out_pin));
            auto out_capacitance_r = m_OpenTimer.load(net_name, split_type::MAX, transition_t::RISE);
            auto out_capacitance_f = m_OpenTimer.load(net_name, split_type::MAX, transition_t::FALL);
            auto out_capacitance = util::femtofarad_t(std::max(out_capacitance_r, out_capacitance_f));
            max_out_capacitance = std::max(out_capacitance, max_out_capacitance);
        }

        auto max_capacitance = m_design.late_timingLibrary().max_capacitance(std_cell);
        if(max_out_capacitance > max_capacitance){
            auto std_cell_name = m_design.standard_cells().name(std_cell);
            auto std_cell_model = std_cell_name.substr(0, std_cell_name.find("_"));
            for(auto option : m_cell_name2options[std_cell_model]){
                double actual_cost = 0.0;
                auto previous_option = m_design.standard_cells().name(m_design.netlist().std_cell(cell_instance));

                m_OpenTimer.repower_cell_instance(*instance_name_it, option);

                actual_cost += timing_cost(*instance_name_it);
                actual_cost += m_power_importance * m_design.late_timingLibrary().cell_leakage_power(m_design.standard_cells().find_cell(option));
                actual_cost += capacitance_cost(*instance_name_it);

                for(auto out_pin : m_digraph.cell_output_pins(m_design.netlist().find_cell_instance(*instance_name_it))){
                    if(!is_connected(out_pin))
                        continue;
                auto net_name = m_design.netlist().name(m_design.netlist().net(out_pin));
                auto out_capacitance_r = m_OpenTimer.load(net_name, split_type::MAX, transition_t::RISE);
                auto out_capacitance_f = m_OpenTimer.load(net_name, split_type::MAX, transition_t::FALL);
                auto out_capacitance = util::femtofarad_t(std::max(out_capacitance_r, out_capacitance_f));
                max_out_capacitance = std::max(out_capacitance, max_out_capacitance);
                }

                std_cell = m_design.netlist().std_cell(cell_instance);
                max_capacitance = m_design.late_timingLibrary().max_capacitance(std_cell);
                if((actual_cost < best_cost) && (max_out_capacitance < max_capacitance)){
                    best_cost = actual_cost;
                    best_option = option;
                }else{
                    m_OpenTimer.repower_cell_instance(*instance_name_it, previous_option);
                }
            }
        }
    }
}

void GateSizing::update_lambda_timing_criticality(std::vector<GateSizing::pin_instance_name_type> inputs, GateSizing::pin_instance_type output_pin){
    for(auto transition : {transition_t::RISE, transition_t::FALL}){
        auto rat_out = m_OpenTimer.rat(m_design.netlist().name(output_pin), split_t::MAX, transition);
        auto at_out = m_OpenTimer.at(m_design.netlist().name(output_pin), split_t::MAX, transition);
        double criticality = at_out/rat_out;
        for(auto inp_name : inputs){
            double lambda_rise = m_digraph.lateLambdaMultiplier(inp_name).rise();
            double lambda_fall = m_digraph.lateLambdaMultiplier(inp_name).fall();
            lambda_rise = (transition == transition_t::RISE) ? lambda_rise * criticality : lambda_rise;
            lambda_fall = (transition == transition_t::FALL) ? lambda_fall * criticality : lambda_fall;
            m_digraph.setLateLambdaMultiplier(inp_name, transitions_type(lambda_fall, lambda_rise));
        }
    }
}

void GateSizing::update_lambdas_pos(){
    for(auto po_name : m_digraph.fanin("SinkNode")){
        if(m_digraph.is_cell_instance(po_name)){
            auto cell_instance = m_design.netlist().find_cell_instance(po_name);
            auto cell_out_pin = worst_arrival_cell_out_pin(cell_instance);
            update_lambda_timing_criticality(m_digraph.cell_input_pins(cell_instance), cell_out_pin);
        }else{
            auto pin_instance = m_design.netlist().find_pin_instance(po_name);
            update_lambda_timing_criticality({m_design.netlist().name(pin_instance)}, pin_instance);
        }
    }
}

GateSizing::pin_instance_type GateSizing::worst_arrival_cell_out_pin(GateSizing::cell_instance_type cell){
    double worst_arrival = 0;
    pin_instance_type worst_pin;
    for(auto out_pin : m_digraph.cell_output_pins(cell)){
        if(!is_connected(out_pin))
            continue;
        for(auto transition : {transition_t::RISE, transition_t::FALL}){
            auto arrival = m_OpenTimer.at(m_design.netlist().name(out_pin), split_t::MAX, transition);
            worst_pin = (arrival > worst_arrival) ? out_pin : worst_pin;
            worst_arrival = std::max(worst_arrival, (double)arrival);
        }
    }
    return worst_pin;
}

void GateSizing::update_lambdas_cells(){
    for(auto instanceName : m_digraph.topology()){
        if(m_digraph.is_po(instanceName))
            continue;
        if(!m_digraph.is_cell_instance(instanceName))
            continue;
        auto cell_instance = m_design.netlist().find_cell_instance(instanceName);
        auto worst_arrival_out_pin = worst_arrival_cell_out_pin(cell_instance);
        auto output_pin_name = m_design.netlist().name(worst_arrival_out_pin);
        for(auto inp_name : m_digraph.cell_input_pins(cell_instance)){
            for(auto transition : {transition_t::RISE, transition_t::FALL}){
                auto arrival_out = m_OpenTimer.arrival_out_cell(inp_name, output_pin_name, transition, split_t::MAX);
                auto worst_arrival_out = m_OpenTimer.at(output_pin_name, split_t::MAX, transition);
                double criticality = arrival_out/worst_arrival_out;

                double lambda_rise = m_digraph.lateLambdaMultiplier(inp_name).rise();
                double lambda_fall = m_digraph.lateLambdaMultiplier(inp_name).fall();
                lambda_rise = (transition == transition_t::RISE) ? lambda_rise * criticality : lambda_rise;
                lambda_fall = (transition == transition_t::FALL) ? lambda_fall * criticality : lambda_fall;
                m_digraph.setLateLambdaMultiplier(inp_name, transitions_type(lambda_fall, lambda_rise));
            }
        }

        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto max_capacitance = m_design.late_timingLibrary().max_capacitance(std_cell);
        auto net_name = m_design.netlist().name(m_design.netlist().net(worst_arrival_out_pin));
        auto out_capacitance_r = m_OpenTimer.load(net_name, split_type::MAX, transition_t::RISE);
        auto out_capacitance_f = m_OpenTimer.load(net_name, split_type::MAX, transition_t::FALL);
        auto out_capacitance = util::femtofarad_t(std::max(out_capacitance_r, out_capacitance_f));
        auto beta_multiplier = m_digraph.lateBetaMultiplier(instanceName);
        beta_multiplier = (beta_multiplier * units::unit_cast<double>(out_capacitance/max_capacitance));
        m_digraph.setLateBetaMultiplier(instanceName, beta_multiplier);
    }
}

void GateSizing::solve_ldp(){
    update_lambdas_pos();
    update_lambdas_cells();
}

void GateSizing::distribute_lambdas(){
    auto topology = m_digraph.topology();
    for(auto instance_name_it = topology.rbegin(); instance_name_it != topology.rend(); instance_name_it++){
        if(!m_digraph.is_cell_instance(*instance_name_it))
            continue;
        auto cell_instance = m_design.netlist().find_cell_instance(*instance_name_it);
        auto cell_out_pins = m_digraph.cell_output_pins(cell_instance);

        double sum_out_rise = 0, sum_out_fall = 0;
        pin_instance_type out_pin;
        for(auto c_out_pin : cell_out_pins){
            if(!is_connected(c_out_pin))
                continue;
            else
                out_pin = c_out_pin;
            auto out_net = m_design.netlist().net(c_out_pin);
            auto fanout_pins = m_digraph.net_targets(out_net);
            for(auto fanout_pin : fanout_pins){
                if(!m_digraph.is_input_pad(fanout_pin) && !m_digraph.is_output_pad(fanout_pin))
                    if(m_digraph.is_sequential_input_pin(fanout_pin))
                        continue;
                auto fanout_pin_name = m_design.netlist().name(fanout_pin);
                auto lateLambda = m_digraph.lateLambdaMultiplier(fanout_pin_name);
                sum_out_rise += lateLambda.rise();
                sum_out_fall += lateLambda.fall();
            }
        }

        if((sum_out_rise == 0) && (sum_out_fall == 0)){
            sum_out_rise = 1;
            sum_out_fall = 1;
            if(m_digraph.fanout(*instance_name_it).size() == 1)
                continue;
        }

        auto cell_input_names = m_digraph.cell_input_pins(cell_instance);
        double sum_inp_rise = 0, sum_inp_fall = 0;
        for(auto inp_name : cell_input_names){
            auto lateLambda = m_digraph.lateLambdaMultiplier(inp_name);
            sum_inp_rise += lateLambda.rise();
            sum_inp_fall += lateLambda.fall();
        }

        for(auto inp_name : cell_input_names){
            auto unateness = m_OpenTimer.arc_unateness(inp_name, m_design.netlist().name(out_pin), split_type::MAX);
            auto lateLambda = m_digraph.lateLambdaMultiplier(inp_name);
            double lambda_fall = (unateness == ot::TimingSense::NEGATIVE_UNATE) ? (lateLambda.fall() / sum_inp_fall) * sum_out_rise : (lateLambda.fall() / sum_inp_fall) * sum_out_fall;
            double lambda_rise = (unateness == ot::TimingSense::NEGATIVE_UNATE) ? (lateLambda.rise() / sum_inp_rise) * sum_out_fall : (lateLambda.rise() / sum_inp_rise) * sum_out_rise;
            m_digraph.setLateLambdaMultiplier(inp_name, transitions_type(lambda_fall, lambda_rise));
        }
    }
}

void GateSizing::print_graph(){
    for(auto instance_name : m_digraph.topology()){
        if(m_digraph.is_cell_instance(instance_name)){
            auto cell_instance = m_design.netlist().find_cell_instance(instance_name);
            auto std_cell = m_design.netlist().std_cell(cell_instance);
            std::cout<<instance_name<<m_design.standard_cells().name(std_cell)<<" b "<<m_digraph.lateBetaMultiplier(instance_name)<<std::endl;
            auto cell_input_names = m_digraph.cell_input_pins(cell_instance);
            for(auto inp_name : cell_input_names){
                auto lambda = m_digraph.lateLambdaMultiplier(inp_name);
                std::cout<<inp_name<<" lr "<<lambda.rise()<<" lf "<<lambda.fall()<<std::endl;
            }

        }else if((instance_name != "SinkNode") || (instance_name != "SourceNode")){
            auto lambda = m_digraph.lateLambdaMultiplier(instance_name);
            std::cout<<instance_name<<" lr "<<lambda.rise()<<" lf "<<lambda.fall()<<" b "<<m_digraph.lateBetaMultiplier(instance_name)<<std::endl;
        }
    }
}

void GateSizing::write_graph(std::string file_name){
    std::filebuf fb;
    fb.open (file_name, std::ios::out);
    std::ostream os(&fb);

    os<<"digraph "<<file_name<<" {"<<std::endl<<std::endl;
    os<<"node [shape=record fontname=Arial];"<<std::endl<<std::endl;

    for(auto instance_name : m_digraph.topology()){
        if(m_digraph.is_cell_instance(instance_name)){
            auto cell_instance = m_design.netlist().find_cell_instance(instance_name);
            auto std_cell = m_design.netlist().std_cell(cell_instance);
            auto std_cell_name = m_design.standard_cells().name(std_cell);
            os<<instance_name<<" [label=\""<<instance_name<<" "<<std_cell_name<<"\\n"
                                           <<"B: "<<m_digraph.lateBetaMultiplier(instance_name);

            auto cell_input_names = m_digraph.cell_input_pins(cell_instance);
            for(auto inp_name : cell_input_names){
                auto lambda = m_digraph.lateLambdaMultiplier(inp_name);
                os<<"\\n"<<inp_name<<" lr "<<lambda.rise()<<" lf "<<lambda.fall();
            }
            os<<"\"]"<<std::endl;

        }else if((instance_name != "SinkNode") || (instance_name != "SourceNode")){
            auto lambda = m_digraph.lateLambdaMultiplier(instance_name);
            os<<instance_name<<" [label=\""<<instance_name<<"\\nB: "<<m_digraph.lateBetaMultiplier(instance_name)<<"\\nlr "<<lambda.rise()<<" lf "<<lambda.fall()<<"\"]"<<std::endl;
        }else if((instance_name != "SinkNode") || (instance_name != "SourceNode"))
            os<<instance_name<<" [label=\""<<instance_name<<"\"]"<<std::endl;
    }

    os<<std::endl;
    for(auto instance_name : m_digraph.topology()){
        if(instance_name == "SinkNode")
            continue;
        auto fanouts = m_digraph.fanout(instance_name);
        os<<instance_name<<" -> {";
        unsigned int i = 0;
        while(i < fanouts.size()){
            os<<fanouts.at(i);
            i++;
            if(i < fanouts.size())
                os<<", ";
        }
        os<<"};"<<std::endl;
    }
    os<<std::endl<<"}";

    fb.close();
}

void GateSizing::print_cost(){
    double cap_cost = 0, t_cost = 0, power_cost = 0;
    for(auto instanceName : m_digraph.topology()){
        if(!m_digraph.is_cell_instance(instanceName))
            continue;

        auto cell_instance = m_design.netlist().find_cell_instance(instanceName);
        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto std_cell_name = m_design.standard_cells().name(std_cell);

        for(auto fanin_name : m_digraph.fanin(instanceName)){
            cap_cost += capacitance_cost(fanin_name);
            if(!m_digraph.is_cell_instance(fanin_name))
                continue;
            auto fanin_instance = m_design.netlist().find_cell_instance(fanin_name);
            t_cost += timing_cost(fanin_name);
        }

        t_cost += timing_cost(instanceName);
        power_cost += m_power_importance * m_design.late_timingLibrary().cell_leakage_power(m_design.standard_cells().find_cell(std_cell_name));
        cap_cost += capacitance_cost(instanceName);
    }
    std::cout<<"CAP: "<<cap_cost<<" TIME: "<<t_cost<<" POWER: "<<power_cost<<std::endl;
}


double GateSizing::power_leakage_cost(){
    double power_cost = 0;
    for(auto instanceName : m_digraph.topology()){
        if(!m_digraph.is_cell_instance(instanceName))
            continue;

        auto cell_instance = m_design.netlist().find_cell_instance(instanceName);
        auto std_cell = m_design.netlist().std_cell(cell_instance);
        auto std_cell_name = m_design.standard_cells().name(std_cell);

        power_cost += m_power_importance * m_design.late_timingLibrary().cell_leakage_power(m_design.standard_cells().find_cell(std_cell_name));
    }
    return power_cost;
}

}
