/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#ifndef OPHIDIAN_GATE_SIZING_DIGRAPH_H
#define OPHIDIAN_GATE_SIZING_DIGRAPH_H

#include <lemon/list_graph.h>
#include <lemon/connectivity.h>
#include <unordered_map>
#include <cassert>
#include <ophidian/timing/Library.h>
#include <ophidian/circuit/Netlist.h>
#include <ophidian/circuit/StandardCells.h>
#include <ophidian/util/Transitions.h>
#include <algorithm>

namespace ophidian::gate_sizing{
class Digraph{
    public:
        using node_type = lemon::ListDigraph::Node;
        using arc_type = lemon::ListDigraph::Arc;
        using digraph_type = lemon::ListDigraph;
        using node_name_type = std::string;
        using pin_name_type = std::string;
        using net_type = ophidian::circuit::Netlist::net_type;
        using pin_instance_type = ophidian::circuit::Netlist::pin_instance_type;
        using node_name_map_type =  lemon::ListDigraph::NodeMap<node_name_type>;
        using topological_map_type = lemon::ListDigraph::NodeMap<int>;
        using timing_library_type = ophidian::timing::Library;
        using transitions_type = ophidian::util::Transitions<double>;

        Digraph() = delete;

        Digraph(const Digraph&) = default;
        Digraph& operator=(const Digraph&) = default;

        Digraph(Digraph&&) = default;
        Digraph& operator=(Digraph&&) = default;

        Digraph(const ophidian::circuit::Netlist &netlist, const ophidian::circuit::StandardCells &stdCells, const ophidian::timing::Library &earlyLibrary, const ophidian::timing::Library &lateLibrary);

        std::vector<node_name_type> primaryInputNames() const;

        std::vector<node_name_type> primaryOutputNames() const;

        std::vector<node_name_type> fanin(const node_name_type &nodeName) const;

        std::vector<node_name_type> fanout(const node_name_type &nodeName) const;

        void initializeMultipliers();

        transitions_type earlyLambdaMultiplier(pin_name_type pinName);

        transitions_type lateLambdaMultiplier(pin_name_type pinName);

        void setLateLambdaMultiplier(pin_name_type pinName, transitions_type t);

        double earlyBetaMultiplier(node_name_type nodeName);

        double lateBetaMultiplier(node_name_type nodeName);

        void setLateBetaMultiplier(node_name_type nodeName, double betaMultiplier);

        std::vector<node_name_type> topology() const;

        inline bool is_cell_instance(node_name_type nodeName) const{
            try{
                m_netlist.find_cell_instance(nodeName);
                return true;
            }catch (std::out_of_range& e){
                return false;
            }
        }

        bool is_po(node_name_type nodeName);

        std::vector<pin_name_type> cell_input_pins(const ophidian::circuit::CellInstance cellInstance) const;

        std::vector<pin_instance_type> cell_output_pins(const ophidian::circuit::CellInstance cellInstance);

        inline bool is_input_pad(const pin_instance_type &pin_instance) const{
            return m_netlist.input(pin_instance) != ophidian::circuit::Input();
        }
        pin_instance_type net_source_pin(net_type net);

        std::vector<pin_instance_type> net_targets(net_type net);

        inline bool is_output_pad(const ophidian::circuit::PinInstance &pin_instance) const{
            return m_netlist.output(pin_instance) != ophidian::circuit::Output();
        }

        inline bool is_sequential_input_pin(const ophidian::circuit::PinInstance &pin_instance) const{
            return m_earlyLibrary.cell_sequential(m_netlist.std_cell(m_netlist.cell(pin_instance))) && !m_earlyLibrary.pin_clock(m_netlist.std_cell_pin(pin_instance));
        }
    private:
        const std::vector<node_name_type> topologicalSort();

        inline bool is_output_cell_pin(const ophidian::circuit::PinInstance &pin_instance) const;
        inline bool is_cell_input_pin(const ophidian::circuit::PinInstance &pin_instance) const;

        digraph_type m_digraph;
        node_type m_sourceNode, m_sinkNode;
        const ophidian::circuit::Netlist &m_netlist;
        const ophidian::circuit::StandardCells &m_std_cells;
        const timing_library_type &m_earlyLibrary, &m_lateLibrary;
        std::unordered_map<pin_name_type, transitions_type> m_name2lambda_early, m_name2lambda_late;
        std::unordered_map<node_name_type,  double> m_name2beta_early, m_name2beta_late;
        std::unordered_map<node_name_type,  node_type> m_name2node;
	    node_name_map_type  m_node2name;
        std::vector<node_name_type> m_topology;
};
}
#endif // OPHIDIAN_GATE_SIZING_DIGRAPH_H

