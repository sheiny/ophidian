/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#ifndef OPHIDIAN_UTIL_TRANSITIONS_H
#define OPHIDIAN_UTIL_TRANSITIONS_H

namespace ophidian::util
{

// Forward declare the class template
template<typename T> class Transitions;

// Declare the function
template <typename T> Transitions<T> operator+(const Transitions<T> &lhs, const Transitions<T> &rhs);

template <typename T> Transitions<T> operator-(const Transitions<T> &lhs, const Transitions<T> &rhs);

// Declare the friend in the class definition
template<typename T>
class Transitions{
    public:
    enum EdgeType{Fall=0, Rise=1};

    Transitions(){
        m_values[Fall] = 0;
        m_values[Rise] = 0;
    }

    Transitions(const Transitions&) = default;
    Transitions& operator=(const Transitions&) = default;

    Transitions(Transitions&&) = default;
    Transitions& operator=(Transitions&&) = default;

    Transitions(T fall, T rise){
        m_values[Fall] = fall;
        m_values[Rise] = rise;
    }

    T aggregate() const{
        return m_values[Fall]+m_values[Rise];
    }

    T rise() const{
        return m_values[Rise];
    }

    T fall() const{
        return m_values[Fall];
    }
    friend Transitions operator+<T>(const Transitions& lhs, const Transitions& rhs);

    friend Transitions operator-<T>(const Transitions& lhs, const Transitions& rhs);

    private:
    T m_values[2];
};

template<typename T>
Transitions<T> operator+(const Transitions<T> &lhs, const Transitions<T> &rhs){
    return Transitions(lhs.m_values[Transitions<T>::Fall]+rhs.m_values[Transitions<T>::Fall], lhs.m_values[Transitions<T>::Rise]+rhs.m_values[Transitions<T>::Rise]);
}

template<typename T>
Transitions<T> operator-(const Transitions<T> &lhs, const Transitions<T> &rhs){
    return Transitions(lhs.m_values[Transitions<T>::Fall]-rhs.m_values[Transitions<T>::Fall], lhs.m_values[Transitions<T>::Rise]-rhs.m_values[Transitions<T>::Rise]);
}
}
#endif // OPHIDIAN_UTIL_TRANSITIONS_H
