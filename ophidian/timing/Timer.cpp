/*
 * Copyright 2017 Ophidian
   Licensed to the Apache Software Foundation (ASF) under one
   or more contributor license agreements.  See the NOTICE file
   distributed with this work for additional information
   regarding copyright ownership.  The ASF licenses this file
   to you under the Apache License, Version 2.0 (the
   "License"); you may not use this file except in compliance
   with the License.  You may obtain a copy of the License at
   http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
   KIND, either express or implied.  See the License for the
   specific language governing permissions and limitations
   under the License.
 */

#include "Timer.h"

namespace ophidian::timing{
    OpenTimer::OpenTimer(ophidian::design::Design & design, ot::Timer &timer):m_timer(timer), m_design(design){
        initial_tns = *m_timer.report_tns();
        initial_wns = *m_timer.report_wns();
        initial_area = *m_timer.report_area();
        initial_power = *m_timer.report_leakage_power();
    }

    float OpenTimer::at(const std::string& pin_name, split_type split_type, transition_type transition_type){
        return *m_timer.report_at(pin_name, split_type, transition_type);
    }

    float OpenTimer::rat(const std::string& pin_name, split_type split_type, transition_type transition_type){
        return *m_timer.report_rat(pin_name, split_type, transition_type);
    }

    float OpenTimer::slew(const std::string& pin_name, split_type split_type, transition_type transition_type){
        return *m_timer.report_slew(pin_name, split_type, transition_type);
    }

    float OpenTimer::slack(const std::string& pin_name, split_type split_type, transition_type transition_type){
        return *m_timer.report_slack(pin_name, split_type, transition_type);
    }

    float OpenTimer::load(const std::string& pin_name, split_type split_type, transition_type transition_type){
        return *m_timer.report_load(pin_name, split_type, transition_type);
    }

    float OpenTimer::leakage_power(){
        return *m_timer.report_leakage_power();
    }

    float OpenTimer::tns(){
        return *m_timer.report_tns();
    }

    float OpenTimer::wns(){
        return *m_timer.report_wns();
    }

    void OpenTimer::update_timing(){
        m_timer.update_timing();
    }

    void OpenTimer::cppr(bool mode){
        m_timer.cppr(mode);
    }

    void OpenTimer::insert_net(std::string net_name){
        m_design.netlist().add_net(net_name);
        m_timer.insert_net(net_name);
    }

    void OpenTimer::insert_cell_instance(std::string cell_instance, std::string standard_cell){
        auto std_cell = m_design.standard_cells().add_cell(standard_cell);
        auto instance = m_design.netlist().add_cell_instance(cell_instance);
        m_design.netlist().connect(instance, std_cell);
        m_timer.insert_gate(cell_instance, standard_cell);
    }

    void OpenTimer::repower_cell_instance(std::string cell_instance, std::string standard_cell){
        auto std_cell = m_design.standard_cells().find_cell(standard_cell);
        auto instance = m_design.netlist().find_cell_instance(cell_instance);
        m_design.netlist().connect(instance, std_cell);
        m_timer.repower_gate(cell_instance, standard_cell);
    }

    void OpenTimer::remove_net(std::string net_name){
        auto net = m_design.netlist().find_net(net_name);
        auto net_pins = m_design.netlist().pins(net);
        for(auto pin : net_pins){
            m_design.netlist().disconnect(pin);
            m_timer.disconnect_pin(m_design.netlist().name(pin));
        }
        m_design.netlist().erase(net);
        m_timer.remove_net(net_name);
    }

    void OpenTimer::remove_cell_instance(std::string cell_instance_name){
        auto cell_instance = m_design.netlist().find_cell_instance(cell_instance_name);
        auto cell_pins = m_design.netlist().pins(cell_instance);
        for(auto pin : cell_pins){
            m_design.netlist().disconnect(pin);
            m_timer.disconnect_pin(m_design.netlist().name(pin));
            m_design.netlist().erase(pin);
        }
        m_design.netlist().erase(cell_instance);
        m_timer.remove_gate(cell_instance_name);
    }

    void OpenTimer::disconnect_pin(std::string pin_instance_name){
        auto pin = m_design.netlist().find_pin_instance(pin_instance_name);
        m_design.netlist().disconnect(pin);
        m_timer.disconnect_pin(pin_instance_name);
    }

    void OpenTimer::connect_pin(std::string pin_instance_name, std::string net_name){
        auto pin = m_design.netlist().find_pin_instance(pin_instance_name);
        auto net = m_design.netlist().find_net(net_name);
        m_design.netlist().connect(net, pin);
        m_timer.connect_pin(pin_instance_name, net_name);
    }

    double OpenTimer::period(){
        return m_timer.clocks().begin()->second.period();
    }

    void OpenTimer::dump_verilog(const std::string& name) const{
        std::filebuf fb;
        fb.open (name, std::ios::out);
        std::ostream os(&fb);
        m_timer.dump_verilog(os, name);
        fb.close();
    }


    OpenTimer::timing_sense OpenTimer::arc_unateness(std::string input_pin_name, std::string output_pin_name, OpenTimer::split_type split){
        auto arcs = timing_arcs(input_pin_name, output_pin_name, split);
        timing_sense unateness;
        for(auto arc : arcs){
            switch(arc.sense.value()){
                    case timing_sense::NEGATIVE_UNATE:
                        unateness = timing_sense::NEGATIVE_UNATE;
                        break;
                    case timing_sense::POSITIVE_UNATE:
                        unateness = timing_sense::POSITIVE_UNATE;
                        break;
                    case timing_sense::NON_UNATE:
                        unateness = timing_sense::NON_UNATE;
                        break;
                    default:
                        break;
            }
        }
        return unateness;
    }

    double OpenTimer::arrival_out_cell(std::string input_pin_name, std::string output_pin_name, OpenTimer::transition_type output_transition, OpenTimer::split_type split){
        auto arcs = timing_arcs(input_pin_name, output_pin_name, split);
        double worst_d = 0, worst_at = 0;
        transition_type input_transition;
        for(auto arc : arcs){
            switch(arc.sense.value()){
                    case timing_sense::NEGATIVE_UNATE:
                        input_transition = (output_transition == transition_type::RISE) ? transition_type::FALL : transition_type::RISE;
                        break;
                    case timing_sense::POSITIVE_UNATE:
                        input_transition = output_transition;
                        break;
                    case timing_sense::NON_UNATE:
                        input_transition = (arc.type.value() == ot::TimingType::RISING_EDGE) ? transition_type::RISE : transition_type::FALL;
                        break;
                    default:
                        break;
            }
            auto out_pin = m_design.netlist().find_pin_instance(output_pin_name);
            auto out_net = m_design.netlist().net(out_pin);
            auto out_load = load(m_design.netlist().name(out_net), split, output_transition);
            auto inp_slew = slew(input_pin_name, split, input_transition);
            auto delay_arc = arc.delay(input_transition, output_transition, inp_slew, out_load);
            if(delay_arc.has_value()){
                worst_d = std::max(worst_d, (double)delay_arc.value());
                worst_at = at(input_pin_name, split, input_transition);
            }
    }
        return worst_at+worst_d;
    }

    double OpenTimer::worst_delay(OpenTimer::t_arcs arcs, OpenTimer::transition_type output_transition, double input_slew, double output_capacitance){
    double worst_d = 0;
    transition_type input_transition;
    for(auto arc : arcs){
        switch(arc.sense.value()){
                case timing_sense::NEGATIVE_UNATE:
                    input_transition = (output_transition == transition_type::RISE) ? transition_type::FALL : transition_type::RISE;
                    break;
                case timing_sense::POSITIVE_UNATE:
                    input_transition = output_transition;
                    break;
                case timing_sense::NON_UNATE:
                    input_transition = (arc.type.value() == ot::TimingType::RISING_EDGE) ? transition_type::RISE : transition_type::FALL;
                    break;
                default:
                    break;
        }
        auto delay_arc = arc.delay(input_transition, output_transition, input_slew, output_capacitance);
        if(delay_arc.has_value())
            worst_d = std::max(worst_d, (double)delay_arc.value());
    }
    return worst_d;
    }

    OpenTimer::t_arcs OpenTimer::timing_arcs(std::string input_pin_name, std::string output_pin_name, split_type split){
        OpenTimer::t_arcs arcs;
        auto out_pin = m_design.netlist().find_pin_instance(output_pin_name);
        auto cell_name = m_design.netlist().name(m_design.netlist().cell(out_pin));
        auto gate_it = m_timer._gates.find(cell_name);
        for(auto arc_ptr : gate_it->second._arcs){
            auto tv = arc_ptr->timing_view();
            auto from_name = arc_ptr->from().name();
            auto to_name = arc_ptr->to().name();
            if(tv[split] && from_name == input_pin_name && to_name == output_pin_name)
                arcs.push_back(*tv[split]);
        }
        return arcs;
    }

    void OpenTimer::dump_spef(const std::string& name) const{
        std::filebuf fb;
        fb.open (name, std::ios::out);
        std::ostream os(&fb);
        m_timer.dump_spef(os);
        fb.close();
    }

    void OpenTimer::dump_output(std::string out_file, std::string lib_name){
        std::filebuf fb;
        fb.open (out_file, std::ios::out);
        std::ostream os(&fb);
        os<<"INITIAL_METRICS "<<lib_name<<std::endl;
        os<<"TNS: "<<initial_tns<<std::endl;
        os<<"WNS: "<<initial_wns<<std::endl;
        os<<"AREA: "<<initial_area<<std::endl;
        os<<"POWER: "<<initial_power<<std::endl;
        os<<"END_METRICS"<<std::endl<<std::endl<<std::endl;

        os<<"INITIAL_METRICS "<<lib_name<<std::endl;
        os<<"TNS: "<<*m_timer.report_tns()<<std::endl;
        os<<"WNS: "<<*m_timer.report_wns()<<std::endl;
        os<<"AREA: "<<*m_timer.report_area()<<std::endl;
        os<<"POWER: "<<*m_timer.report_leakage_power()<<std::endl;
        os<<"END_METRICS"<<std::endl<<std::endl<<std::endl;

        os<<"BEGIN_ECO"<<std::endl;
        for(auto cell_it = m_design.netlist().begin_cell_instance(); cell_it != m_design.netlist().end_cell_instance(); cell_it++){
            auto cellName = m_design.netlist().name(*cell_it);
            auto stdCell = m_design.netlist().std_cell(*cell_it);
            auto stdCellName = m_design.standard_cells().name(stdCell);

            os<<"repower_gate "<<cellName<<" "<<stdCellName<<std::endl;
        }
        os<<"END_ECO"<<std::endl;
        fb.close();
    }
}
